from dataclasses import asdict, dataclass


@dataclass
class TiposComedias: 
    uuid: str
    name: str
    lugar_origen : str
    description: str

    # a = {"name": "Comedia griega", "lugar_origen": "Grecia", "description": "se origina en la Antigua Grecia y forma parte del teatro clásico"}

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass
class Comediantes: 
    uuid : str
    nombres : str
    tipo_comedia : TiposComedias
    pais_origen : str
    nombre_comedia : str

   # b = {"nombres":"Aristófanes", "tipo_comedia":"id_mongo", "lugar_origen":"Grecia", "nombre_comedia":"Las avispas"}


    def to_dict(self) -> dict:
        return asdict(self)

    @property
    def get_tipo_comedia(self): 
        return self.tipo_comedia.name


