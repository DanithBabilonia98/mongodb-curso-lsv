from bson.objectid import ObjectId
import sys

sys.path.append("..")
from databases.client import MongoLsv


mongo = MongoLsv()

print(mongo.list_dbs())
print(mongo.list_collections(db_name="comedias"))

print(
    mongo.create_new_record_in_collection(
        db_name="comedias",
        collection="tipos_comedias",
        record={
            "name": "Comedia del arte",
            "lugar_origen": "Italia",
            "description": "significa literalmente teatro interpretado por personas del arte, es decir, por actores profesionales",
        },
    )
)

print(
    mongo.create_new_record_in_collection(
        db_name="comedias",
        collection="comediantes",
        record={
            "nombres": "Helen Fielding",
            "tipo_comedia": ObjectId("62ae0bcd20d56fd4e6603a92"),
            "lugar_origen": "Reino Unido",
            "nombre_comedia": "El diario de Bridget Jones",
        },
    )
)

print(mongo.get_records_from_collection(db_name="comedias", collection="comediantes"))
print(
    mongo.get_records_from_collection(db_name="comedias", collection="tipos_comedias")
)
