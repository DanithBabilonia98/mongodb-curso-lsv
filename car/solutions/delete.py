from bson.objectid import ObjectId

import sys
sys.path.append("..")
from databases.client import MongoLsv

mongo = MongoLsv() 

print(
    mongo.delete_record_in_collection(
        db_name="comedias",
        collection="comediantes",
        record_id="62ae0f5eea1d6ce74fff0e49",
    )
)

print(
    mongo.delete_record_in_collection(
        db_name="comedias",
        collection="tipos_comedia",
        record_id="62ae0ae595c891c86eb45c01",
    )
)


print(
    mongo.get_records_from_collection(
        db_name="comedias", collection="comediantes"
    )
)


print(
    mongo.get_records_from_collection(
        db_name="comedias", collection="tipos_comedias"
    )
)