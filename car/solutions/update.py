from bson.objectid import ObjectId

import sys
sys.path.append('..')
from databases.client import MongoLsv 

mongo = MongoLsv() 

print(
    mongo.update_record_in_collection(
        db_name="comedias",
        collection="tipos_comedias",
        record_query={"_id": ObjectId('62ae0ae595c891c86eb45c01')},
        record_new_value={"name": "Comedias pastorales o pastoriles",
        "lugar origen":"",
        "descripcion": "Dedicada a la bucólica vida en el campo, con amores entre pastorcillos o campesinos."},
    )
)

print(
    mongo.update_record_in_collection(
        db_name="comedias",
        collection="comediantes",
        record_query={"_id": ObjectId('62ae146f2a9bad62bca7e9b3')},
        record_new_value={"name": "Moliere",
        "tipo_comedia": ObjectId('62ae0ae595c891c86eb45c01'),
        "pais_origen": "Francia",
        "nombre_comedia": "El enfermo imaginario"},
    )
)

print(
    mongo.get_records_from_collection(
        db_name="comedias", collection="tipos_comedias"
    )
)

print(
    mongo.get_records_from_collection(
        db_name="comedias", collection="comediantes"
    )
)