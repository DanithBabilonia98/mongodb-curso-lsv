# Final curso de MONGODB

Examen final

 - **Cada estudiante tiene su propio repositorio con su respectivo tema**
 - **Se crearan una interface de las respectivas tablas ejemplo Departamento / Municipio:**
 - **Se crearan en la consola de mongodb la base de datos:**
 - **Haran un archivo por cada operacion del crud ejemplo:**

```sh
    # Register new Record in Collection
    $ python3 register.py
    $ python3 update.py
    $ python3 delete.py
```

- **Haran un archivo por cada operacion del crud ejemplo:**
- **Siempre llamar get_records_from_collection al final de cada archivo:**
- **Anexar Resultados:**
- **Crear un PullRequest en sus propias branch con la respectiva solucion:**


https://gitlab.com/ErickCBarrios/mongodb-curso-lsv
```sh
    Erick C Barrios = departamento / municipio
```

https://gitlab.com/Zetien/mongodb-curso-lsv 
```sh
    Jorge Zetien = compania / empleado
```


https://gitlab.com/Mary-Anaya/mongodb-curso-lsv
```sh
    Maria Anaya = profesion / empleado
```


https://gitlab.com/oballestas/mongo-project ->
```sh
   Oscar Ballestas = lenguaje_de_programacion / programador
```

https://gitlab.com/jsierrabravo/mongodb-curso-lsv -> 
```sh
Juan Sierra = trabajo / oferta_de_trabajo
```

https://gitlab.com/andresmolinares/mongodb-curso-lsv -> 
```sh
Andres Felipe Molinares Bolaños = tipo de verduras / verduras
```

https://gitlab.com/leangell/mongodb-curso-lsv -> 
```sh
Leandro Meza Vasquez  =  tipo de droguerias / drogueria
```

https://gitlab.com/JJPayares/mongodb-curso-lsv -> 
```sh
Juan José Payares Trocha =    tipo de alimentos / alimentos
```

https://gitlab.com/ronaldrpc/mongodb-curso-lsv -> 
```sh
Ronald Paternina =    tipo de cuenta bancaria / cuenta bancaria
```

https://gitlab.com/IsaacBenavidesTorres/mongo-curso-lsv -> 
```sh
IsaacBenavides =    pais / banco
```

https://gitlab.com/DeijoseDevelop/mongodb_cariv -> 
```sh
Deiver Vazquez =    persona / vehiculos
```

https://gitlab.com/herlyn25/mongo-db-hcc - > 
```sh
Herly Castillo =    partes de bicicleta / bicicleta
```

https://gitlab.com/Ricardo_lsv-tech/moongodb-curso-lsv -> 
```sh
Ricardo Jaramillo Acevedo =    tipos de video juegos / videojuegos
```

https://gitlab.com/steeven10/mongo_db/-/tree/devel ->
```sh
steeven Altamiranda =    deportes / deportista
```

https://gitlab.com/mauroLSV/mongodb-curso-lsv -> 
```sh
Mauricio Arias =    universidades / profesores
```

https://gitlab.com/ZuleydisBarriosP/mongodb-curso-lsv/ -> 
```sh
Zuleydis Barrios =    universidades / estudiantes
```

https://gitlab.com/AmandaQ16/mongo -> 
```sh
Amanda Quintana =    universidades / asignaturas
```

https://gitlab.com/luispayaresj/mongo_db -> 
```sh
Luis Payares Joly =    tipo de cocinas / cocinas
```

https://gitlab.com/Yilbert/mongodb-curso-lsv -> 
```sh
Yilber Molina =    tipo de negocio / negocio
```

https://gitlab.com/angelicapmorales/mongodb-curso-lsv -> 
```sh
Angelica Morales =    tipos de medio de comunicacion / medios de comunicacion
```

https://gitlab.com/VelaidesCJ/mongodb-curso-lsv -> 
```sh
Jair Calderon =    pais / animales_pertenencientes_a_pais
```

https://gitlab.com/Asanchez04/mongo_project -> 
```sh
Alan Sanchez Tovar =    tema / documental
```

https://gitlab.com/Jota.alvarez/mongodb-2 -> 
```sh
Jhonatan Alvarez De la hoz =    artes marciales / artistas marciales
```

https://gitlab.com/ejnpuni/mongo/-/tree/mongo -> 
```sh
Ever Navarrro Padilla =    genero de anime / anime
```

https://gitlab.com/DanithBabilonia98/mongodb-curso-lsv/-/tree/develop -> 
```sh
Danith Babilonia Meza =    tipo de comedia / comediantes
```

https://gitlab.com/Andyfer91/mongodb-curso-lsv -> 
```sh
Andres Fernando Garcia Meza =    genero de peliculas / peliculas
```

https://gitlab.com/JuanPabloLA/mongodb-curso-lsv -> 
```sh
Juan Pablo Lara Angulo =    tipo de laboratorio / laboratorio
```

https://gitlab.com/Joseph517/mongodb-curso-lsv -> 
```sh
Alvaro Vergara =    pais / monumento_famoso
```

https://gitlab.com/Tilve/mongodb-curso-lsv -> 
```sh
Luis Tilve =    pais / actor_famoso
```